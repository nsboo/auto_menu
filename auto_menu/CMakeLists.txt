get_filename_component(ProjectId ${CMAKE_CURRENT_SOURCE_DIR} NAME)
project(${ProjectId})

file(GLOB DEPS CONFIGURE_DEPENDS "src/*")
set(ALL_DEPS ${ALL_DEPS} ${DEPS})

add_executable(${PROJECT_NAME}
  ${ALL_DEPS}
)
target_include_directories(${PROJECT_NAME}
  PUBLIC
    include
)

target_link_libraries(${PROJECT_NAME}
  PRIVATE
    fmt::fmt
	ftxui::screen
	ftxui::dom
	ftxui::component
)
#include <functional> // for function
#include <memory>     // for shared_ptr, allocator, __shared_ptr_access
#include <string>     // for string, basic_string
#include <vector>     // for vector
#include <array>
#include <deque>
#include <fstream>
#include <sstream>
#include <fmt/core.h>
#include <stdlib.h>
#include "ftxui/component/captured_mouse.hpp"     // for ftxui
#include "ftxui/component/component.hpp"          // for Slider, Checkbox, Vertical, Renderer, Button, Input, Menu, Radiobox, Toggle
#include "ftxui/component/component_base.hpp"     // for ComponentBase
#include "ftxui/component/screen_interactive.hpp" // for Component, ScreenInteractive
#include "ftxui/dom/elements.hpp"                 // for separator, operator|, Element, size, xflex, text, WIDTH, hbox, vbox, EQUAL, border, GREATER_THAN
#include <random>

namespace
{
    constexpr int num_countries = 5;
    constexpr int num_ingridients = 6;
    constexpr int num_methods = 5;
    constexpr size_t bool_size = num_countries + num_ingridients + num_methods;
}
using namespace ftxui;
void resultPage(std::deque<bool> &countries, std::deque<bool> &ingridients, std::deque<bool> &methods, std::deque<std::array<bool, num_countries>> &rest_countries, std::deque<std::array<bool, num_ingridients>> &rest_ingridients, std::deque<std::array<bool, num_methods>> &rest_methods, std::deque<std::string> &all_list);                              //결과화면 출력 함수
void checkAll(std::string &name, std::deque<bool> &boxes);                                                                                                                                                                                                                                                                                                     // name이 전체 선택이면 boxes를 모두 true로, name이 전체 해재면 boxes를 모두 false로
std::string getResult(std::deque<std::string> &list);                                                                                                                                                                                                                                                                                                          //랜덤식당을 반환해주는 함수
void file2restaurant(std::string &path, std::deque<std::array<bool, num_countries>> &rest_countries, std::deque<std::array<bool, num_ingridients>> &rest_ingridients, std::deque<std::array<bool, num_methods>> &rest_methods, std::deque<std::string> &all_list);                                                                                             //파일을 식당리스트로 변환
void makeList(std::deque<bool> &countries, std::deque<bool> &ingridients, std::deque<bool> &methods, std::deque<std::array<bool, num_countries>> &rest_countries, std::deque<std::array<bool, num_ingridients>> &rest_ingridients, std::deque<std::array<bool, num_methods>> &rest_methods, std::deque<std::string> &all_list, std::deque<std::string> &list); //전체 식당리스트에서 조건에 맞는 식당리스트로 변환
//파일에서 식당을 받아오는 함수

Component Wrap(std::string name, Component component)
{
    return Renderer(component, [name, component]
                    { return vbox({
                                 text(name) | center,
                                 separator(),
                                 component->Render() | xflex,
                             }) |
                             xflex; });
}

int main(int argc, const char *argv[])
{
    auto screen = ScreenInteractive::FitComponent();

    // -- Checkbox ---------------------------------------------------------------
    std::deque<bool> countries(num_countries, false);     //체크박스용
    std::deque<bool> ingridients(num_ingridients, false); //체크박스용
    std::deque<bool> methods(num_methods, false);         //체크박스용
    std::deque<std::array<bool, num_countries>> rest_countries;
    std::deque<std::array<bool, num_ingridients>> rest_ingridients;
    std::deque<std::array<bool, num_methods>> rest_methods;
    std::string path = "DB.txt";                                                     // DB이름 (+경로)
    std::deque<std::string> all_list;                                                //전체 가게명 리스트
    file2restaurant(path, rest_countries, rest_ingridients, rest_methods, all_list); //파일을 리스트로 변환

    auto checkbox_countries = Container::Vertical({
        Checkbox("한식", &countries.at(0)),
        Checkbox("중식", &countries.at(1)),
        Checkbox("일식", &countries.at(2)),
        Checkbox("동남아", &countries.at(3)),
        Checkbox("양식", &countries.at(4)),
    });
    auto checkbox_ingredients = Container::Vertical({
        Checkbox("밥", &ingridients.at(0)),
        Checkbox("면", &ingridients.at(1)),
        Checkbox("소고기", &ingridients.at(2)),
        Checkbox("돼지고기", &ingridients.at(3)),
        Checkbox("닭고기", &ingridients.at(4)),
        Checkbox("생선", &ingridients.at(5)),
    });
    auto checkbox_methods = Container::Vertical({
        Checkbox("구이", &methods.at(0)),
        Checkbox("튀김", &methods.at(1)),
        Checkbox("볶음", &methods.at(2)),
        Checkbox("찜", &methods.at(3)),
        Checkbox("국", &methods.at(4)),
    });

    checkbox_countries = Wrap("국가", checkbox_countries);
    checkbox_ingredients = Wrap("재료", checkbox_ingredients);
    checkbox_methods = Wrap("조리법", checkbox_methods);
    // -- Button -----------------------------------------------------------------
    std::string quit = "Quit";
    // std::function<void()> on_button_clicked_;
    auto quit_button = Button(&quit, screen.ExitLoopClosure());
    std::string countries_check_all = "전부 선택";
    std::string ingridients_check_all = "전부 선택";
    std::string methods_check_all = "전부 선택";
    auto countries_check_all_button = Button(&countries_check_all,
                                             [&]()
                                             { checkAll(countries_check_all, countries); });
    auto ingridients_check_all_button = Button(&ingridients_check_all,
                                               [&]()
                                               { checkAll(ingridients_check_all, ingridients); });
    auto methods_check_all_button = Button(&methods_check_all,
                                           [&]()
                                           { checkAll(methods_check_all, methods); });
    std::string result_page = " GO!";
    auto result_page_button = Button(&result_page, [&]
                                     { resultPage(countries, ingridients, methods, rest_countries, rest_ingridients, rest_methods, all_list); });
    // button = Wrap("Button", button);

    // -- Layout -----------------------------------------------------------------
    auto buttons = Container::Horizontal({countries_check_all_button, ingridients_check_all_button, methods_check_all_button});

    auto layout = Container::Vertical({
        Container::Horizontal({checkbox_countries, checkbox_ingredients, checkbox_methods}),
        buttons,
        result_page_button,
        quit_button,
    });

    auto component = Renderer(layout, [&]
                              { return vbox({
                                           text("Auto Menu") | center,
                                           separator(),
                                           hbox({checkbox_countries->Render() | size(WIDTH, EQUAL, 14), separator(),
                                                 checkbox_ingredients->Render() | size(WIDTH, EQUAL, 14), separator(),
                                                 checkbox_methods->Render() | size(WIDTH, EQUAL, 14)}),
                                           separator(),
                                           hbox({countries_check_all_button->Render() | size(WIDTH, EQUAL, 15) | xflex,
                                                 ingridients_check_all_button->Render() | size(WIDTH, EQUAL, 15) | xflex,
                                                 methods_check_all_button->Render() | size(WIDTH, EQUAL, 15) | xflex}) |
                                               center,
                                           vbox({result_page_button->Render() | size(WIDTH, EQUAL, 15) | center,
                                                 quit_button->Render() | size(WIDTH, EQUAL, 15) | center}),
                                       }) |
                                       xflex | size(WIDTH, GREATER_THAN, 40) | border; });

    screen.Loop(component);

    return 0;
} //메인 끝

void file2restaurant(std::string &path, std::deque<std::array<bool, num_countries>> &rest_countries, std::deque<std::array<bool, num_ingridients>> &rest_ingridients, std::deque<std::array<bool, num_methods>> &rest_methods, std::deque<std::string> &all_list) //파일을 식당리스트로 변환
{
    std::string line;
    std::ifstream file("DB.txt");
    if (file.is_open())
    {
        getline(file, line);
        int res_index = 0; //식당인덱스
        while (getline(file, line))
        {
            std::string word;
            std::istringstream linebuf(line);
            linebuf >> word;
            all_list.emplace_back(word);
            bool type;
            int index = 0; // 식당 분류 인덱스
            int i = 0;
            int j = 0;
            int k = 0;
            while (linebuf >> type)
            {
                if (index < 5)
                {
                    rest_countries[res_index][i] = type;
                    i++;
                }
                else if (index < 11)
                {
                    rest_ingridients[res_index][j] = type;
                    j++;
                }
                else
                {
                    rest_methods[res_index][k] = type;
                    k++;
                }
                index++;
            }
            res_index++;
        }
    }
    // fmt::print("{}\n", all_list.size());
    // for (int i = 0; i < 3; i++)
    // {
    //     fmt::print("{} ", all_list[i]);
    //     for (int j = 0; j < 5; j++)
    //     {
    //         fmt::print("{} ", rest_countries[i][j]);
    //         fmt::print("{} ", rest_ingridients[i][j]);
    //         fmt::print("{} ", rest_methods[i][j]);
    //     }
    //     fmt::print("\n");
    // } //테스트출력용
}
void resultPage(std::deque<bool> &countries, std::deque<bool> &ingridients, std::deque<bool> &methods, std::deque<std::array<bool, num_countries>> &rest_countries, std::deque<std::array<bool, num_ingridients>> &rest_ingridients, std::deque<std::array<bool, num_methods>> &rest_methods, std::deque<std::string> &all_list) //결과화면 출력 함수
{
    auto screen = ScreenInteractive::FitComponent();
    std::string result = "결과";
    std::string another_restaurant = "다른 가게";
    std::string config_again = "재설정";
    std::string quit = "Quit";
    std::deque<std::string> list;
    makeList(countries, ingridients, methods, rest_countries, rest_ingridients, rest_methods, all_list, list);
    result = getResult(list);
    // result = getResult(list);
    auto another_restaurant_button = Button(&another_restaurant, [&]
                                            { result = getResult(list); });
    auto config_again_button = Button(&config_again, screen.ExitLoopClosure());

    auto quit_button = Button(&quit, [&]
                              { exit(0); });
    auto layout = Container::Vertical({
        another_restaurant_button,
        config_again_button,
        quit_button,
    });
    auto component = Renderer(layout, [&]
                              { return vbox({
                                           text("\n") | center,
                                           text("결과") | center,
                                           text("\n") | center,
                                           text("\n") | center,
                                           text("\n") | center,
                                           text(result) | center,
                                           text("\n") | center,
                                           text("\n") | center,
                                           text("\n") | center,
                                           text("\n") | center,
                                           text("\n") | center,
                                           another_restaurant_button->Render() | size(WIDTH, EQUAL, 15) | center,
                                           config_again_button->Render() | size(WIDTH, EQUAL, 15) | center,
                                           quit_button->Render() | size(WIDTH, EQUAL, 15) | center,
                                       }) |
                                       size(WIDTH, EQUAL, 45) | size(HEIGHT, EQUAL, 20) | border; });
    screen.Loop(component);
}
void makeList(std::deque<bool> &countries, std::deque<bool> &ingridients, std::deque<bool> &methods, std::deque<std::array<bool, num_countries>> &rest_countries, std::deque<std::array<bool, num_ingridients>> &rest_ingridients, std::deque<std::array<bool, num_methods>> &rest_methods, std::deque<std::string> &all_list, std::deque<std::string> &list) //전체 식당리스트에서 조건에 맞는 식당리스트로 변환
{
    list.clear(); //리스트 초기화
    for (int rest_index = 0; rest_index < all_list.size(); rest_index++)
    {
        bool co_check = false;
        bool ing_check = false;
        bool met_check = false;
        for (int co_index = 0; co_index < num_countries; co_index++)
        {
            if (rest_countries[rest_index][co_index] == true && countries[co_index] == true)
            {
                co_check = true;
                break;
            }
        }
        if (co_check == true)
        {
            for (int ing_index = 0; ing_index < num_ingridients; ing_index++)
            {
                if (rest_ingridients[rest_index][ing_index] == true && ingridients[ing_index] == true)
                {
                    ing_check = true;
                    break;
                }
            }
            if (ing_check == true)
            {
                for (int met_index = 0; met_index < num_methods; met_index++)
                {
                    if (rest_methods[rest_index][met_index] == true && methods[met_index] == true)
                    {
                        met_check = true;
                        list.emplace_back(all_list[rest_index]);
                        break;
                    }
                }
            }
        }
    }
}

std::string getResult(std::deque<std::string> &list) //조건에 맞는 랜덤식당을 반환해주는 함수
{
    if (list.size() == 0)
    {
        return "조건에 맞는 가게가 없습니다.";
    }
    else
    {
        std::random_device r;
        std::default_random_engine e1(r());
        std::uniform_int_distribution<int> uni(0, list.size() - 1);
        int rand = uni(e1);
        return list[rand];
    }
}

void checkAll(std::string &name, std::deque<bool> &boxes) // name이 전체 선택이면 boxes를 모두 true로, name이 전체 해재면 boxes를 모두 false로
{
    if (name == "전부 선택")
    {
        name = "전부 해제";
        for (bool &check : boxes)
        {
            check = true;
        }
    }
    else
    {
        name = "전부 선택";
        for (bool &check : boxes)
        {
            check = false;
        }
    }
}